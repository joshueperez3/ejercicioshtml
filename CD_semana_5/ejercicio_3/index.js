function quiz() {
    let numMaquina = Math.round(Math.random() * 100);
    for (let intentos = 5; intentos > 0; intentos--) {
        let numIntroducido = parseInt(
            prompt("Introduce un número entre el 0 y el 100.")
        );
        if (numMaquina === numIntroducido) {
            alert("¡Has acertado, enhorabuena!");
            break;
        } else if (numMaquina > numIntroducido) {
            alert("El número es mayor.");
        } else {
            alert("El número es menor.");
        }
    }
}

quiz();
