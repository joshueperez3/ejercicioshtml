const pizzas = [
    "margarita",
    "cuatro quesos",
    "prosciutto",
    "carbonara",
    "barbacoa",
    "tropical",
];

function combine(pizzas) {
    const combinations = [];
    for (let i = 0; i < pizzas.length; i++) {
        for (let x = 0; x < pizzas.length; x++)
            if (i === x || i > x) {
                combinations.slice();
            } else {
                combinations.push(pizzas[i] + " y " + pizzas[x]);
            }
    }

    return combinations;
}

console.log(combine(pizzas));
