const people = {
    Maria: 20,
    Ana: 14,
    Luis: 16,
    Pepe: 35,
    Manuel: 50,
    Teresa: 12,
    Daniel: 27,
    Irene: 23,
    Alex: 10,
};

function funcion(name, age) {
    if (age > 17) {
        console.log(name, "es mayor de edad.");
    } else {
        console.log(name, "es menor de edad.");
    }
}

for (let propiedad in people) {
    funcion(propiedad, people[propiedad]);
}
