let nombre = "Marta";
let edad = 27;

if (edad < 12) {
    console.log("A " + nombre + " le corresponde el descuento infantil.");
} else if (edad < 30) {
    console.log("A " + nombre + " le corresponde el descuento juvenil.");
} else if (edad > 60) {
    console.log("A " + nombre + " le corresponde el descuento de jubilados.");
} else {
    console.log("A " + nombre + " no le corresponde ningún descuento.");
}

// Como pone en el enunciado <12, <30 y >60 no he puesto el <=12, <=30 y >=60 porque supongo que no es necesario.
